import { Routes } from '@angular/router';
import { HomeComponent } from './home';

import { DataResolver } from './app.resolver';
import { PageNotFoundComponent } from './404/404.component';

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  // { path: 'product', loadChildren: './product/product.module#ProductModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: '**', component: PageNotFoundComponent },
];
