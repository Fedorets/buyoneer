import { Component } from '@angular/core';

@Component({
  selector: 'buy-404',
  styleUrls: [ './404.component.scss' ],
  templateUrl: './404.component.html'
})
export class PageNotFoundComponent {}
