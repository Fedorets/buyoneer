import { Component } from '@angular/core';

@Component({
  selector: 'buy-sign-up',
  styleUrls: [ './sign-up.component.scss' ],
  templateUrl: './sign-up.component.html'
})
export class SignUpComponent {}
