import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { routes } from './auth.routes';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent
  ],
  exports: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})

export class AuthModule {}
