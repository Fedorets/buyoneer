import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddProductComponent } from './add-product/add-product.component';
import { DetailComponent } from './detail/detail.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductItemComponent } from './product-list/product-item/product-item.component';
import { ProductService } from './shared/product.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [],
  declarations: [
    AddProductComponent,
    DetailComponent,
    ProductListComponent,
    ProductItemComponent
  ],
  providers: [
    ProductService
  ]
})

export class ProductModule {}
