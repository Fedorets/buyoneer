import { Component } from '@angular/core';

@Component({
  selector: 'buy-detail',
  styleUrls: [ './detail.component.scss' ],
  templateUrl: './detail.component.html'
})
export class DetailComponent {}
