import { Component } from '@angular/core';

@Component({
  selector: 'buy-product-list',
  styleUrls: [ './product-list.component.scss' ],
  templateUrl: './product-list.component.html'
})
export class ProductListComponent {}
