import { Component } from '@angular/core';

@Component({
  selector: 'buy-product-item',
  styleUrls: [ './product-item.component.scss' ],
  templateUrl: './product-item.component.html'
})
export class ProductItemComponent {}
