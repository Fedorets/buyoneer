import { Component } from '@angular/core';

@Component({
  selector: 'buy-home',
  styleUrls: [ './home.component.scss' ],
  templateUrl: './home.component.html'
})
export class HomeComponent {}
